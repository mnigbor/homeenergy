//----------------------------------------------------------------------
// Users route. Services to manage user accounts go here
//
// Copyright (C) 2018 ModusInfo, LLC
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License at https://www.gnu.org/licenses
//    for more details.
//----------------------------------------------------------------------
var express = require('express');
var router = express.Router();
var winston = require('winston');

/* GET user information */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

// Create or update a user account
router.post('/', function(req, res, next) {
  winston.log('info', 'users POST');

  // Get the user ID and password from the request
  var username = req.body.username;
  var password = req.body.password;
  var account = null;
  var userAccount = registerUser( req, res.locals.connection );

  // Process the promise of account information here
  userAccount.then(function(result) {
    account = result;
    res.json(response);
  }, function(err) {
    var response = {
      status: 500,
      message: 'Problem registering the user',
      token: null,
      expires: null
    };
    res.json(response);
  });
});

//---------------------------------------------------------------------------
// Function: registerUser
//---------------------------------------------------------------------------
function registerUser( req, con ) {
  var account = {
    uid: null,
    username: null,
    passwordhash: null
  }

  // Return promise of user account information
  // Database connection setup in app.js and passed via response object
  var prom = new Promise(function(resolve, reject) {
    // Check user input
    var username = req.body.username;
    var password = req.body.password;
    var firstname = req.body.fname;
    var lastname = req.body.lname;
    var email = req.body.email;

    // Make sure account name is available

    // Generate a hash of the user's password
    var hash = bcrypt.hashSync(myPlaintextPassword, saltRounds);    
    con.query('inser into users values (?, ?, ?, ?, ?, ?)', 
      [username, firstname, lastname, email, null, null, hash] , function (error, results, fields) {
      if (error) {
        // Error running query
        reject(error);
      }
      else {
        // Query ran, return the accout info
        resolve( account );
      }
    });
  });

  return prom;
};

module.exports = router;
