//----------------------------------------------------------------------------
// history router module
// GET: returns a table of currentreadings suitable for Google charts
//
// Copyright (C) 2018 ModusInfo, LLC
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License at https://www.gnu.org/licenses
//    for more details.
//----------------------------------------------------------------------------
var passport = require('../app');
var express = require('express');
var router = express.Router();
var moment = require('moment');
var bcrypt = require('bcryptjs');
var Cryptr = require('cryptr');
var cryptr = new Cryptr('{522809CE-873D-4342-95D7-13C58F01E7E5}');
var winston = require('winston');

// Returns the most recent current (amperage) reading
router.get('/', function (req, res, next) {
  winston.log('info', 'Entering currenthistory GET');

  // debug code - remove before publishing
  res.locals.token = { user: 'mnigbor'};

  var dataPromise = getCurrentHistory(res.locals.token.user, res.locals.connection);

  // Process the promise of data here
  dataPromise.then(function(result) {
    winston.log('info', 'Exiting current GET succesfully');
    data = result;
    res.json(data);
  }, function(err) {
    winston.log('error','Error getting current information: ' + err);
    var response = {
      status: 500,
      message: 'Problem getting current information: ' + err,
      token: null,
      expires: null
    };
    res.status(500).json(response);
  });
});

//------------------------------------------------------------------------------
// Function: getLastestCurrent
// Takes user ID and database connection
// Returns promise of data, which is the most recent current reading we have
// -----------------------------------------------------------------------------
function getCurrentHistory(username, con) {
  // Return promise of current data
  // Database connection setup in app.js and passed via response object
  //var byHourSql = 'select hour(readingdate) as hour, ROUND(AVG(current1 + current2),2) as ave_current from currentreadings group by hour(readingdate) where username = ?';
  var last24Sql = "SELECT CAST(DATE_FORMAT(readingdate, '%H%i') AS UNSIGNED) AS Minutes, " +
    "ROUND(current1 + current2, 2) AS Current " +
    "FROM currentreadings WHERE readingdate > TIMESTAMP(CURRENT_DATE()) AND username = 'mnigbor' " +
    "ORDER BY Minutes";

  var prom = new Promise(function(resolve, reject) {
    con.query(last24Sql, [username], function (error, results, fields) {
      var graphData = [];
      row = ['Minutes', 'Current'];
      graphData.push(row);
  
      if (error) {
        // Error running query
        reject(error);
      }
      else {
        // Query ran. Process rows
        if(results.length > 0) {
          for (var i = 0; i < results.length; i++) { 
            var hour = results[i].Minutes;
            var current = results[i].Current;
            row = [hour, current];
            graphData.push(row);
          };

          resolve( graphData );
        }
        else {
          // No results, return empty set
          resolve( graphData );
        }
      }
    });
  });

  return prom;
};

module.exports = router;