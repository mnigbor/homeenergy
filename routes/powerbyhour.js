//----------------------------------------------------------------------------
// powerbyhour router module
// GET: returns a table of kWhr groups by hour of day
// suitable for Google charts
//
// Copyright (C) 2018 ModusInfo, LLC
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License at https://www.gnu.org/licenses
//    for more details.
//----------------------------------------------------------------------------
var passport = require('../app');
var express = require('express');
var router = express.Router();
var moment = require('moment');
var bcrypt = require('bcryptjs');
var Cryptr = require('cryptr');
var cryptr = new Cryptr('{522809CE-873D-4342-95D7-13C58F01E7E5}');
var winston = require('winston');

// Returns the most recent current (amperage) reading
router.get('/', function (req, res, next) {
  winston.log('info', 'Entering powerbyhour GET');

  // debug code - remove before publishing
  res.locals.token = { user: 'mnigbor'};

  var dataPromise = getPowerByHour(res.locals.token.user, res.locals.connection);

  // Process the promise of data here
  dataPromise.then(function(result) {
    winston.log('info', 'Exiting current GET succesfully');
    data = result;
    res.json(data);
  }, function(err) {
    winston.log('error', 'Error getting current information: ' + err);
    var response = {
      status: 500,
      message: 'Problem getting current information: ' + err,
      token: null,
      expires: null
    };
    res.status(500).json(response);
  });
});

//------------------------------------------------------------------------------
// Function: getPowerByHour
// Takes user ID and database connection
// Returns promise of data, which is average power consumption by hour of day
// for the last month
// -----------------------------------------------------------------------------
function getPowerByHour(username, con) {
  // Return promise of current data
  // Database connection setup in app.js and passed via response object
  var powerByHourSQL = "SELECT hour(readingdate) as Hour,ROUND(AVG(current1 + current2)*0.120,2) AS KWHr " +
    " FROM currentreadings " +
    " WHERE username = 'mnigbor' " +
    " AND readingdate >= now() - INTERVAL 1 MONTH " +
    " GROUP BY HOUR(readingdate)";

    var graphData = [];
    row = ['Hour', 'kWh'];
    graphData.push(row);

  var prom = new Promise(function(resolve, reject) {
    con.query(powerByHourSQL, [username], function (error, results, fields) {
      if (error) {
        // Error running query
        reject(error);
      }
      else {
        // Query ran. Process rows
        if(results.length > 0) {
          for (var i = 0; i < results.length; i++) { 
            var hour = results[i].Hour;
            var current = results[i].KWHr;
            row = [hour, current];
            graphData.push(row);
          };

          resolve( graphData );
        }
        else {
          // No results, return empty set
          resolve( graphData );
        }
      }
    });
  });

  return prom;
};

module.exports = router;