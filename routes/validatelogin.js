//-----------------------------------------------------------------------
// validatelogin
// POST: takes user Id and password and returns token if valid
//
// Copyright (C) 2018 ModusInfo, LLC
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License at https://www.gnu.org/licenses
//    for more details.
//------------------------------------------------------------------------
var passport = require('../app');
var express = require('express');
var router = express.Router();
var moment = require('moment');
var bcrypt = require('bcryptjs');
var Cryptr = require('cryptr');
var mysql = require('mysql');
var cryptr = new Cryptr('{522809CE-873D-4342-95D7-13C58F01E7E5}');

// GET verb used for testing only
router.get('/', function (req, res, next) {
  console.log('validateLogin GET');
  res.send('validatelogin GET working');
});

// POST verb: Login form posts here for validation 
// Returns error JSON or an encrypted user object
router.post('/', function (req, res, next) {
  console.log('validatelogin POST');

  // Get the user ID and password from the request
  var username = req.body.username;
  var password = req.body.password;
  var account = null;
  var accountPromise = accountLookup( username, res.locals.connection );

  // Process the promise of account information here
  accountPromise.then(function(result) {
    account = result;
    if( account.username != 'unknown') {
      // Compare the password from the user with the hash from the database
        var match = bcrypt.compareSync(password, account.passwordhash);
      if (match) {
        // Calculate token expiration time (30 minutes from now)
        var expireTime = moment().add(30, 'm');
  
        // Build up the token object
        var token = {
          uid: account.uid,
          user: account.username,
          roles: ['admin'],
          expires: expireTime.toDate()
        };
  
        // Encrypt the token
        var tokenJSON = JSON.stringify(token);
        var encryptedToken = cryptr.encrypt(JSON.stringify(token));
  
        // Build up response object and send it with HTTP 200 OK
        var response = {
          status: 200,
          message: 'OK',
          token: encryptedToken,
          expires: expireTime
        };
        res.send( response );
      }
      else {
        // Password hash doesn't match.
        var response = {
          status: 401,
          message: 'Invalid user name or password',
          token: null,
          expires: null
        };
        res.status(401).send(response);
      };
    }
    else {
      // Username not found
      var response = {
        status: 401,
        message: 'Invalid user name or password',
        token: null,
        expires: null
      };
      res.json(response);
    };
  }, function(err) {
    var response = {
      status: 500,
      message: 'Problem getting login information',
      token: null,
      expires: null
    };
    res.json(response);
  });

});

//---------------------------------------------------------------------------
// Function: accountLookup
// Takes username and database connection 
// Returns promise of user account information
// If user is not found the username 'unknown' is returned
//---------------------------------------------------------------------------
function accountLookup( username, con ) {
  var account = {
    uid: 1,
    username: 'mnigbor',
    passwordhash: '$2a$10$S0IXeMji2kLW9Qpp1.yP/OHnps9TQy.Jbn85F/ak5kDXDbcXpx6Cu'
  }

  // Return promise of user account information
  // Database connection setup in app.js and passed via response object
  var prom = new Promise(function(resolve, reject) {
    con.query('select * from users where username = ?', [username], function (error, results, fields) {
      if (error) {
        // Error running query
        reject(error);
      }
      else {
        // Query ran. Process the first row
        if(results.length > 0) {
          account.uid = results[0].uid;
          account.username = results[0].username;
          account.passwordhash = results[0].hash;
          resolve( account );
        }
        else {
          // User not found
          account.uid = -1;
          account.username = 'unknown';
          account.passwordhash = null;
          resolve( account );
        }
      }
    });
  });

  return prom;
}

module.exports = router;