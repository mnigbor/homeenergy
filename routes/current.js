//----------------------------------------------------------------------------
// current router module
// POST: takes amperage reading from raspberry PI, inserts it into database
//
// Copyright (C) 2018 ModusInfo, LLC
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License at https://www.gnu.org/licenses
//    for more details.
//----------------------------------------------------------------------------
var passport = require('../app');
var express = require('express');
var router = express.Router();
var moment = require('moment');
var bcrypt = require('bcryptjs');
var Cryptr = require('cryptr');
var cryptr = new Cryptr('{522809CE-873D-4342-95D7-13C58F01E7E5}');
var winston = require('winston');

// Returns the most recent current (amperage) reading
router.get('/', function (req, res, next) {
  winston.log('info', 'Entering current GET');
  var dataPromise = getLatestCurrent(res.locals.token.user, res.locals.connection);

  // Process the promise of data here
  dataPromise.then(function(result) {
    winston.log('info', 'Exiting current GET succesfully');
    data = result;
    res.json(data);
  }, function(err) {
    winston.log('warn','Error getting current information: err');
    var response = {
      status: 500,
      message: 'Problem getting current information',
      token: null,
      expires: null
    };
    res.status(500).json(response);
  });
});

// Rasberry PI posts current (amperage) readings here
// Returns error JSON or an encrypted user object
router.post('/', function (req, res, next) {
  winston.log('info','Entering current POST');
  
  var postDataPromise = postCurrentData(req.body, res.locals.token.user, res.locals.connection);

  postDataPromise.then((result) => {
    // Resolve
    winston.log('info', 'Exiting current POST succesfully');
    data = result;
    res.json(data);
  }, function(err){
    // Reject
    winston.log('error', 'ERROR inserting current data');
    var response = {
      status: 500,
      message: 'Error inserting current data',
    };
    res.status(500).json(response);
  }).catch((error) => {
    // Exception in promise code
    winston.log('error', 'ERROR inserting current data');
    var response = {
      status: 500,
      message: 'Error inserting current data'
    };
    res.status(500).json(response);
  });
});

//------------------------------------------------------------------------------
// Function: getLastestCurrent
// Takes user ID and database connection
// Returns promise of data, which is the most recent current reading we have
// -----------------------------------------------------------------------------
function getLatestCurrent(username, con) {
  // Return promise of current data
  // Database connection setup in app.js and passed via response object
  var sql = 'select round(current1 + current2, 2) as current from currentreadings where username = ? order by readingdate desc limit 1';
  var prom = new Promise(function(resolve, reject) {
    con.query(sql, [username], function (error, results, fields) {
      if (error) {
        // Error running query
        reject(error);
      }
      else {
        // Query ran. Process the first row
        if(results.length > 0) {
          var current = results[0].current;

          var data = [
            ['Label', 'Value'],
            ['Amps', current],
          ]; 

          resolve( data );
        }
        else {
          // No data
          var data = [
            ['Label', 'Value'],
            ['Amps', 0.0],
          ]; 
          resolve( data );
        }
      }
    });
  });

  return prom;
};

//------------------------------------------------------------------------------
// Function: postCurrentData
// Inserts a current data record into the database
//------------------------------------------------------------------------------
function postCurrentData(data, username, con) {
  console.log('Entering postCurrentData');
  var sql = 'insert into currentreadings (username, readingdate, current1, current2) values( ?, ?, ?, ?)';

  var prom = new Promise((resolve, reject) => {
    con.query(sql, [username, data.readingdate, data.current1, data.current2], function (error, results, fields) {
      if (error) {
        // Error running query
        console.log('Error in postCurrentData');
        reject(error.message);
      }
      else {
        // Query ran. 
        console.log('Exit postCurrentData: Success');
        var response = { status: 'OK' };
        resolve(response);
      }
    });
  });
  return prom;
};

module.exports = router;