//----------------------------------------------------------------------
// JavaScript for dashboard page of a Home Energy monitoring system
//
// Copyright (C) 2018 ModusInfo, LLC
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License at https://www.gnu.org/licenses
//    for more details.
//----------------------------------------------------------------------
(function($) {
  "use strict"; // Start of use strict
  Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
  Chart.defaults.global.defaultFontColor = '#292b2c';
  
  // Google charts
  google.charts.load('current', {'packages':['corechart','gauge', 'bar', 'line']});
  google.charts.setOnLoadCallback(drawCharts);

  window.setTimeout(drawAmperage, 10000);

  // Configure tooltips for collapsed side navigation
  $('.navbar-sidenav [data-toggle="tooltip"]').tooltip({
    template: '<div class="tooltip navbar-sidenav-tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
  })
  // Toggle the side navigation
  $("#sidenavToggler").click(function(e) {
    e.preventDefault();
    $("body").toggleClass("sidenav-toggled");
    $(".navbar-sidenav .nav-link-collapse").addClass("collapsed");
    $(".navbar-sidenav .sidenav-second-level, .navbar-sidenav .sidenav-third-level").removeClass("show");
  });
  // Force the toggled class to be removed when a collapsible nav link is clicked
  $(".navbar-sidenav .nav-link-collapse").click(function(e) {
    e.preventDefault();
    $("body").removeClass("sidenav-toggled");
  });
  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .navbar-sidenav, body.fixed-nav .sidenav-toggler, body.fixed-nav .navbar-collapse').on('mousewheel DOMMouseScroll', function(e) {
    var e0 = e.originalEvent,
      delta = e0.wheelDelta || -e0.detail;
    this.scrollTop += (delta < 0 ? 1 : -1) * 30;
    e.preventDefault();
  });
  // Scroll to top button appear
  $(document).scroll(function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });
  // Configure tooltips globally
  $('[data-toggle="tooltip"]').tooltip()
  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function(event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    event.preventDefault();
  });
})(jQuery); // End of use strict

function drawCharts() {
  drawAmperage();
  drawCurrentHistory();
  drawPowerByHour();
};

function buildURL() {
  var url = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
  return url;
};

//----------------------------------------------------------------
// Function: drawAmperage()
// Draws the amerage gauge
//----------------------------------------------------------------
function drawAmperage() {

  $.ajax({
    url: buildURL() + '/current',
    type: 'GET',
    dataType: 'json',
    success: function( data ) {
      var dataTable = google.visualization.arrayToDataTable( data);
  
      var options = {
        min: 0, max: 100,
        width: 400, height: 120,
        redFrom: 90, redTo: 100,
        yellowFrom: 80, yellowTo: 90,
        minorTicks: 5
      };

      var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
      chart.draw(dataTable, options);
    
    },
    error: function(err) { 
      // Process error (likely security)
      if (err.status === 401) {
        // Either not logged in or token expired, go to login
        $('#loginModal').modal('show');
      }
      else {
        alert('Error getting data from server!'); 
      };
    }
  });
};

function drawCurrentHistory() {
  $.ajax({
    url: buildURL() + '/currenthistory',
    type: 'GET',
    dataType: 'json',
    success: function( data ) {
      var dataTable = google.visualization.arrayToDataTable(data);
  
      var options = {
        height: 300,
        chartArea:{width:'90%',height:'70%'},
        curveType: 'function',
        explorer: {},
        legend: {
          position: 'in'
        },
        hAxis: {
          title: 'Time',
          ticks: [
            {v:200, f:'0200'},
            {v:400, f:'0400'},
            {v:600, f:'0600'},
            {v:800, f:'0800'},
            {v:1000, f:'1000'},
            {v:1200, f:'1200'},
            {v:1400, f:'1400'},
            {v:1600, f:'1600'},
            {v:1800, f:'1800'},
            {v:2000, f:'2000'},
            {v:2200, f:'2200'},
            {v:1400, f:'2400'}
          ],
          textStyle : { fontSize: 10 },
          showTextEvery : 1,
          slantedText : true
        },
        vAxis: {
          title: 'Current (amps)',
          textStyle : { fontSize: 10 }
        }
      };

      if( dataTable.getNumberOfRows() > 0 ) {
        var chart = new google.visualization.LineChart(document.getElementById('historyChartContainer'));
        chart.draw(dataTable, options);
      }
    
    },
    error: function(err) { 
      // Process error (likely security)
      if (err.status === 401) {
        // Either not logged in or token expired, go to login
        $('#loginModal').modal('show');
      }
      else {
        alert('Error getting data from server!'); 
      };
    }
  });
};

function drawPowerByHour() {
  $.ajax({
    url: buildURL() + '/powerbyhour',
    type: 'GET',
    dataType: 'json',
    success: function( data ) {
      var dataTable = google.visualization.arrayToDataTable(data);
  
      var options = {
        height: 300,
        chartArea:{width:'90%',height:'70%'},
        curveType: 'function',
        legend: {
          position: 'in'
        },
        hAxis: {
          title: 'Hour',
          textStyle : { fontSize: 10 },
          gridlines : {count: 4}
        },
        vAxis: {
          title: 'Power (kWh)',
          textStyle : { fontSize: 10 }
        }
      };
    
      if (dataTable.getNumberOfRows() > 0 ) {
        var chart = new google.visualization.ColumnChart(document.getElementById('powerByHourContainer'));
        chart.draw(dataTable, options);
  
        // Process this data into TOD usage
        setTODUsage( dataTable );

      };
    },
    error: function(err) { 
      // Process error (likely security)
      if (err.status === 401) {
        // Either not logged in or token expired, go to login
        $('#loginModal').modal('show');
      }
      else {
        alert('Error getting data from server!'); 
      };
    }
  });
};

function validateLogin() {

  var username = $("#username").val();
  var password = $("#password").val();
  var login = { username, password };

  // TODO: Make sure these aren't empty

  // Send request to the server.
  $.post("validatelogin", login, function (user) {
      if (user.err) {
          // Error
          $("#message").text(user.err);
      }
      else {
          // Success, save token in cookies, hide login dialog
          var expDate = new Date(user.expires);
          createCookie('token', user.token, expDate, '/', null);
          $('#loginModal').modal('hide');
          location.reload();
      }
  })
  .fail(function(response) {
    var msg = response.responseJSON.message;
    $("#message").text(msg); 
  });
};

function logout() {
  deleteCookie('token', '/', null);
  $('#logoutModal').modal('hide');
  $('#loginModal').modal('show');
};

function createCookie(name, value, expires, path, domain) {
  var cookie = name + "=" + escape(value) + ";";

  if (expires) {
    // If it's a date
    if(expires instanceof Date) {
      // If it isn't a valid date
      if (isNaN(expires.getTime()))
       expires = new Date();
    }
    else
      expires = new Date(new Date().getTime() + parseInt(expires) * 1000 * 60 * 60 * 24);

    cookie += "expires=" + expires.toGMTString() + ";";
  }

  if (path)
    cookie += "path=" + path + ";";
  if (domain)
    cookie += "domain=" + domain + ";";

  document.cookie = cookie;
};

function getCookie(name) {
  var regexp = new RegExp("(?:^" + name + "|;\s*"+ name + ")=(.*?)(?:;|$)", "g");
  var result = regexp.exec(document.cookie);
  return (result === null) ? null : result[1];
};

function deleteCookie(name, path, domain) {
  // If the cookie exists
  if (getCookie(name))
    createCookie(name, "", -1, path, domain);
};

function setTODUsage( dataTable ) {
  // dataTable has hour-by-hour usage.  
  // Add groups of these together to get peak, offpeak and shoulder usage
  var offPeak = 0;
  var peak = 0;
  var shoulder = 0;

  for (i=0; i<dataTable.getNumberOfRows() -1 ; i++) {
    var theTime = dataTable.getValue(i, 0);
    var theValue = dataTable.getValue(i, 1);

    if( theTime < 9 ) {
      // midnight to 9am: Offpeak - Part 1
      offPeak += theValue;
    }

    if (theTime >= 9 && theTime < 14 ) {
       // 9am to 2pm - Shoulder - Part 1
       shoulder += theValue;
    }

    if (theTime >= 14 && theTime < 18) {
      // 2pm to 6pm - Peak
      peak += theValue;
    }

    if (theTime >= 18 && theTime < 21) {
      // 6pm to 9pm - Shoulder - Part 2
      shoulder += theValue;
    }

    if (theTime >= 21 && theTime < 24) {
      // 9pm to 12am - Off Peak - Part 2
      offPeak += theValue;
    }

  }
    // Set these values into the page
    offPeak = offPeak.toFixed(2);
    $('#offPeak').text( offPeak);
    peak = peak.toFixed(2);
    $('#peak').text( peak );
    shoulder = shoulder.toFixed(2);
    $('#shoulder').text( shoulder );

}