//-----------------------------------------------------------------------
// sessionhelper.js
// Helper functions for logging in and out
//
// Copyright (C) 2018 ModusInfo, LLC
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License at https://www.gnu.org/licenses
//    for more details.
//------------------------------------------------------------------------
function validateLogin() {

    var username = $("#username").val();
    var password = $("#password").val();
    var login = { username, password };
  
    // TODO: Make sure these aren't empty
  
    // Send request to the server.
    $.post("validatelogin", login, function (user) {
        if (user.err) {
            // Error
            $("#message").text(user.err);
        }
        else {
            // Success, save token in cookies, hide login dialog
            var expDate = new Date(user.expires);
            createCookie('token', user.token, expDate, '/', null);
            $('#loginModal').modal('hide');
            location.reload();
        }
    })
    .fail(function(response) {
      var msg = response.responseJSON.message;
      $("#message").text(msg); 
    });
  };
  
  function logout() {
    deleteCookie('token', '/', null);
    $('#logoutModal').modal('hide');
    $('#loginModal').modal('show');
  };
  
  function createCookie(name, value, expires, path, domain) {
    var cookie = name + "=" + escape(value) + ";";
  
    if (expires) {
      // If it's a date
      if(expires instanceof Date) {
        // If it isn't a valid date
        if (isNaN(expires.getTime()))
         expires = new Date();
      }
      else
        expires = new Date(new Date().getTime() + parseInt(expires) * 1000 * 60 * 60 * 24);
  
      cookie += "expires=" + expires.toGMTString() + ";";
    }
  
    if (path)
      cookie += "path=" + path + ";";
    if (domain)
      cookie += "domain=" + domain + ";";
  
    document.cookie = cookie;
  };
  
  function getCookie(name) {
    var regexp = new RegExp("(?:^" + name + "|;\s*"+ name + ")=(.*?)(?:;|$)", "g");
    var result = regexp.exec(document.cookie);
    return (result === null) ? null : result[1];
  };
  
  function deleteCookie(name, path, domain) {
    // If the cookie exists
    if (getCookie(name))
      createCookie(name, "", -1, path, domain);
  };