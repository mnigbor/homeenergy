#!/usr/bin/env node
//----------------------------------------------------------------------
// Node.js application that implements services and web site for
// a Home Energy monitoring system
//
// Copyright (C) 2018 ModusInfo, LLC
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License at https://www.gnu.org/licenses
//    for more details.
//----------------------------------------------------------------------

// Basic app setup (Express, logging, path, request parsers, sessions)
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var Cryptr = require('cryptr');
var cryptr = new Cryptr('{522809CE-873D-4342-95D7-13C58F01E7E5}');
var mysql = require('mysql');
var https = require('https');
var fs = require('fs');
var winston = require('winston');
winston.default.transports.console.level='info';

// Read config file
winston.log('info', 'Reading configuration file');
var content;
try {
  content = fs.readFileSync('config.json');
}
catch (err) {
  winston.log('error', 'Error reading configuration', err);
}
var cfg = JSON.parse(content);

// Setup routes
var users = require('./routes/users');
var current = require('./routes/current');
var validatelogin = require('./routes/validatelogin');
var currenthistory = require('./routes/currenthistory');
var powerbyhour = require('./routes/powerbyhour');

var app = express();

process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  winston.log('error','unhandledRejection', error.message);
});

var con = mysql.createConnection({
  host: cfg.dbhost,
  user: cfg.dblogin,
  password: cfg.dbpassword,
  port: cfg.dbport
});

// Connect and set the schema
con.connect(function(err) {
  if (err) {
    winston.log('error', 'Unable to connect to database', err);
    process.exit();
  }
  winston.log('info', 'Database connected');
  con.query("use homeenergy", function (err, result) {
    if (err) {
      winston.log('error', 'use homeenergy command failed', err);
      process.exit();
    }
    winston.log('info','HomeEnergy schema selected');
    con.query("select * from users", function (err, result) {
      if (err) {
        winston.log('error', 'Test query failed', err);
        process.exit();
      }
      winston.log('info', 'Test query successful');
    });
  });
});

// Middleware function adds database connection to the response locals
// (so route handlers can access the database)
app.use(function(req, res, next) {
  res.locals.connection = con;
  next();
});

// view engine setup  (Not used much in this app)
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));
app.use('/users', users);
app.use('/validatelogin', validatelogin);
app.use('/current', validateToken, current);
app.use('/currenthistory', validateToken, currenthistory);
app.use('/powerbyhour', validateToken, powerbyhour);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
//module.exports = winston;

var options = {
  ca: fs.readFileSync('./cert/auth.ca-bundle'),
  key: fs.readFileSync('./cert/server.private.key'),
  cert: fs.readFileSync('./cert/certificate.crt')
};

if (!cfg.useSSL) {
  app.listen(cfg.listenport);
  winston.log('info', 'Listening on port ' + cfg.listenport);
}
else {
  https.createServer(options, app).listen(443);

  // Redirect from http port 80 to https
  var http = require('http');
  http.createServer(function (req, res) {
      res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
      res.end();
  }).listen(80);
  winston.log('info', 'Listening on port 443');
};


//-------------------------------------------------------------------------
// validateToken: Node middleware function
// Validates token in the request.
//-------------------------------------------------------------------------
function validateToken(req, res, next) {
  winston.log('info', 'Entering validateToken');

  if (req.cookies.token === undefined) {
    // No authentication token
    var respObj = {
      status: 401,
      message: 'Not logged in',
      token: null,
      expires: null
    };
    winston.log('info', 'Token not supplied with request');
    res.status(401).json(respObj);
  }
  else {
    // We have a token, see if it's any good
    var encryptedToken = req.cookies.token;

    // Decrypt the token
    try {
      var tokenStr = cryptr.decrypt(encryptedToken);
      var token = JSON.parse(tokenStr);
    
      // If the decription worked, look at the expiration timestamp
      var expDate = new Date(token.expires);
      if( Date() > expDate) {
        var respObj = {
          status: 401,
          message: 'Token expired',
          token: null,
          expires: null
        };
        winston.log('info', 'Token expired');
        res.status(401).json(respObj);
      }
      else {
        // Decryption worked and token is valid
        winston.log('info', 'Token is valid');
        // Add the token to the response object so route handlers have access
        res.locals.token = token;
        next();
      }
  
    }
    catch(err) {
      var respObj = {
        status: 401,
        message: 'Token not valid',
        token: null,
        expires: null
      };
      winston.log('warn', 'Token decryption failed');
      next(respObj);
    } 
  }
};

