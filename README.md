# About HomeEnergy #

HomeEnergy is one of two parts needed to run the HomeEnergy - Pi home energy monitor documented at

https://www.hackster.io/michael-nigbor/homeenergy-pi-cecfdf 

The other portion consists of some Python code that runs on a Raspberry PI.  It posts data to this web application.  See the Hackster page (above) for more information.

### HomeEnergy Web Application ###

This repository contains the HomeEnergy web application. 

Version 0.5 - Functional but not finished

### Setup ###

#### Prerequisites ####

* Node (https://nodejs.org/en/)
* Express (https://expressjs.com/) 
* Crypter (https://www.npmjs.com/package/cryptr)
* bcrypt (https://www.npmjs.com/package/bcrypt)
* winston (https://www.npmjs.com/package/winston)
* mysql (https://www.mysql.com/)
 
#### Clone this repo ####
* git clone https://mnigbor@bitbucket.org/mnigbor/homeenergy.git

#### Database configuration ####

##### Create database #####
Log into your MySQL database using the command line or the gui.  Create a user HomeEnergy.  Create a new database called homeenergy.  Give the HomeEnergy user full rights to the databas.

##### Create empty tables #####
There are just two tables in the database, users and currentreadings.

Run the create_users.sql and create_currentreadings.sql script in the root of this repo.

##### Creating web site login user(s) #####

Unfortunately, web pages to create and manage user accounts aren't done yet.  This means you'll have to 
manually add them to the users table via SQL. This is straightforward except for the password hash.

Instead of storing passwords in the database, the password hash is stored. When a user logs in, the password the user provided is hashed and then compared with the hash stored in the database. 

You can make a password hash with a little test program or by stopping this program in the debugger where login happens.  To build a hash use

```
bcrypt.hash(myPlaintextPassword, saltRounds, function(err, hash) {
  // Store hash in your password DB.
});
```

Store this hash in the user's record in the hash column.

##### Config File #####
There is one file missing from the repo that you will need to create manually.  This file contains configuration information including user ids, passwords to get into the databaase.

```
{
    "dbhost": "localhost",
    "dblogin": "HomeEnergy",
    "dbpassword": "DBPASSWORD_HERE",
    "dbport": 3306,
    "listenport": 443, 
    "useSSL": true,
    "loglevel": "info"
}
```
##### SSL Configuration #####
If the server can be reached from the internet, you should configure it to run SSL on port 443.  To do that, follow the instructions at this link to get a certificate: https://medium.com/@nileshsingh/everything-about-creating-an-https-server-using-node-js-2fc5c48a8d4e 

Once you receive your certificate:

* Create a directory called cert
* Place your private key, certificate bundle and certificate signing request into the cert directory.
* Make sure the file names match what the code is looking for:
..* auth.ca-bundle is the bundle file you got from the CA
..* server.private.key is the private key you generated before you requested a certificate
..* certificate.crt is the certificate file
* Modify the config.json file, setting listenport to 443 and useSSL to true.

Of course, you can also fix the code where these files are read to use your filenames, too. This is in app.js.

### Who do I talk to? ###

* You can reach me via Hackster.io (https://www.hackster.io/michael-nigbor/homeenergy-pi-cecfdf)