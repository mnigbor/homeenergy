CREATE TABLE `currentreadings` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `readingdate` datetime NOT NULL,
  `current1` float NOT NULL,
  `current2` float NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=124855 DEFAULT CHARSET=utf8;